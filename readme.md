# About
This is a collection of code in NodeJS which I've created and released.

# Contents
### phpSessionMiddleware
This is a middleware for Express.js which lets you access PHP session data. I
created it for a site which has a NodeJS data provider in addition to the PHP
back end so the user could be authenticated in NodeJS after logging in via PHP.

It's currently only one-way (Reading from PHP to NodeJS).

# Contact
You can contact me via my web page [Patabugen.co.uk](http://www.patabugen.co.uk/contact "Contact")