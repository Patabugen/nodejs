// Middleware for Express.js allowing (read only) access to PHP
// sessions. You have two methods for getting them, if you're on
// the same server you can read them from the file system, if you
// are not then you can get them from a URL on the PHP server.
//
// You also have the option to recieve the sessionID either by
// reading the cookies (if you're on the same domain) or by
// having it passed in as a param.
// 
// @author Sami Greenbury <http://www.patabugen.co.uk>
// @version 0.1 January 2014
// 
// Todo:
//	- Have the config passed in, rather than coded in this file.
//
// Contributiosn welcome

var fs = require('fs');
var PHPUnserialize = require('php-unserialize');
var http = require('http');

var phpSessionMiddleware = {};

phpSessionMiddleware.config = {
	phpSessionType: 'cookie', // url|file currently supported. You can specify an array to have fallbacks
	phpSessionIdSource: 'cookie', // cookie|param (param will check GET or POST for phpSessionParamName (below))
	phpSessionFileLocation: 'c:/wamp/tmp',
	phpSessionCookieName: 'PHPSESSID',
	phpSessionParamName: 'PHPSESSID',
	phpSessionUrl: undefined, // the URL from which to request a JSON string of the session. &sessionId= is appended to the end of it 
	logging: false // Whether to log to the console what's going on. Handy for debugging. Errors are always logged.
};

// MY CONFIG. This should be passed into the constructor.
phpSessionMiddleware.config.logging = false;
phpSessionMiddleware.config.phpSessionIdSource = ["param", "cookie"];
var envrionment = process.argv[2];
if (envrionment == 'production') {
	phpSessionMiddleware.config.phpSessionHost = "example.org";
} else {
	phpSessionMiddleware.config.phpSessionHost = "localhost";
}
phpSessionMiddleware.config.phpSessionPort = 80;
phpSessionMiddleware.config.phpSessionPath = "/session.php?secretKey=CHANGE_ME";
phpSessionMiddleware.config.phpSessionType = "url";

phpSessionMiddleware.handle = function(req, res, next) {
	var sessionData = {};

	var phpSessionId = this.getSessionId(req);
	// If a session isn't set, we can't load it
	if (phpSessionId === undefined || phpSessionId.length < 1) {
		this.setSessionData(req, {}, next);
	} else {
		// Get the session data and handle it in a generic callback
		this.getSessionData(phpSessionId, function(err, data){
			if (err) {
				next(err);
			} else {
				if (data === undefined) {
					phpSessionMiddleware.log("Session Data is Undefined");
					phpSessionMiddleware.setSessionData(req, {}, next);
				} else {
					data._session_id = phpSessionId;
					phpSessionMiddleware.setSessionData(req, data, next);
				}
			}
		});
	}
};

phpSessionMiddleware.getSessionId = function(req, source){
	// We could have a switch here to get the ID from a cookie, or from GET
	var sessionId;

	// If the source is an array, loop through it
	if (source === undefined && typeof this.config.phpSessionIdSource == 'object') {
		for (var i in this.config.phpSessionIdSource) {
			var thisSource = this.config.phpSessionIdSource[i];
			sessionId = this.getSessionId(req, thisSource);
			// IF we found a session ID, set it so it can be returned
			if (sessionId !== '' && sessionId !== undefined) {
				sessionId = sessionId;
				break;
			}
		}
	} else {
		// If it's not an array, work with what we have
		if (source === undefined) {
			source = this.config.phpSessionIdSource;
		}
		this.log("Checking for a sessionID in " + source);

		// If we're looking in a cookie
		if (source == 'cookie') {
			if (req.cookies === undefined) {
				throw new Error('You must enable cookie Parser middleware to use phpSessionMiddleware with cookie source');
			}
			sessionId = req.cookies[this.config.phpSessionCookieName];
		} else if (source == 'param') {
			// If we're looking in a param...
			if (req.body === undefined) {
				throw new Error('You must enable body Parser middleware to use phpSessionMiddleware with cookie source');
			}
			if (req.body.hasOwnProperty(this.config.phpSessionParamName)) {
				sessionId = req.body[this.config.phpSessionParamName];
			} else {
				sessionId = '';
			}
		}
		if (sessionId !== '') {
			this.log("Found Session ID : " + sessionId);
		} else {
			this.log("No Session ID Found");
		}
	}
	return sessionId;
};

/**
*	Gets the session data, be that from a file or some other place (other places
*	are not yet supported, mind). This function doesn't do the unserialising
*
*	Callback is function(err, data)
*
**/
phpSessionMiddleware.getSessionData = function(phpSessionId, callback){
	if (this.config.phpSessionType == 'file') {
		var filePath = this.config.phpSessionFileLocation + '/sess_'+phpSessionId;
		var sessionExists = fs.existsSync(filePath);
		if(sessionExists){ 
			this.log('Session File Found : ' + filePath);
			// If the session exists, load it
			fs.readFile(filePath, function(err, data){
				data = data.toString();
				var sessionData = PHPUnserialize.unserializeSession(data);
				callback(err, sessionData);
			});
		} else {
			// If the session doesn't exist, the session is empty
			this.log('Session File Not Found : ' + filePath);
			callback(null, '');
		}
	} else if (this.config.phpSessionType == 'url') {
		var url = this.config.phpSessionHost + ':' + this.config.phpSessionPort + this.config.phpSessionPath + '&sessionId=' + phpSessionId;
		this.log('Requesting Session from : ' + url);
		var options = {
			hostname: this.config.phpSessionHost,
			port: this.config.phpSessionPort,
			path: this.config.phpSessionPath + '&sessionId=' + phpSessionId,
		};
		var req = http.request(options, function(res){
			if (res.statusCode != 200) {
				this.log('Status Code ' + res.statusCode + ' is not valid from URL ' + url);
				next('Error Getting Session from URL');
				return;
			}
			res.setEncoding('utf8');
			res.on('data', function (data) {
				var sessionData = JSON.parse(data);
				callback(null, sessionData);
			});
		});
		// Error handling for the request
		req.on('error', function(e) {
			phpSessionMiddleware.log('problem with request: ' + e.message, 'error');
			callback('Error getting session by URL');
		});
		req.end();
	} else {
		throw new Error('Unknown phpSessionType : ' + this.config.phpSessionType);
	}
};

phpSessionMiddleware.log = function(msg, level) {
	if (this.config.logging || level == 'error') {
		console.log('[phpSessionMiddleware] ' + msg);
	}
};

phpSessionMiddleware.dir = function(msg, level) {
	if (this.config.logging || level == 'error') {
		console.dir(msg);
	}
};

phpSessionMiddleware.setSessionData = function(req, data, next) {
	this.log("Setting Session Data To : ");
	this.dir(data);
	req.session = data;
	next();
};

module.exports = phpSessionMiddleware;
