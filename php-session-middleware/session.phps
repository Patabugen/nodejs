<?php
/**
 *  Use this file on your PHP server if you want to use the URL
 *  method to get the session data.
 **/
if ($_GET['secretKey'] != 'CHANGE_THIS_TO_MATCH_YOUR_CONFIG') {
	exit;
}
if(!isset($_GET['sessionId'])) {
	exit;
}
$sessionId = $_GET['sessionId'];
$oldSessionId = session_id(); 
session_id($sessionId);
session_start();
echo json_encode($_SESSION);
if ($oldSessionId != '') {
	session_id($oldSessionId);
}
